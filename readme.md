# Seeder

# Changelog
## 1.0
+ Klasa seeder z funkcja progressbarow do seederow

### Installation

Dodajemy w composer.json w sekcji required
```
#!php
"netinteractive/seeder": "@dev",
```

w respositories:
```
#!php
{
    "type": "git",
    "url": "git@bitbucket.org:niteam/laravel-seeder"
},
```

Klase Seedera przeciazamy
```
#!php
class DatabaseSeeder extends \Netinteractive\Seeder\NISeeder
```
